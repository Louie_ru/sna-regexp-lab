echo "Remove comments:"
grep -v -P '^[\t ]*#[^!]' script.bash
echo

echo "URIs:"
grep -E "([A-Za-z]*://[A-Za-z0-9-]+)|((src)|(href)[ \t]*=[ \t]*.)" uris.html
echo

echo "Dutch numbers:"
grep -E "(((\+)|(00))(31)|(0)([ ]?[0-9]){9})|(\(?0[ ]?2[ ]?0\)?([ ]?[0-9]){7})" dutch_and_non-dutch.lst
echo

echo "Visa:"
grep -E " 4(([- ])*[0-9]){12}(([- ])*[0-9]){3}?" cardnumbers.json
echo
